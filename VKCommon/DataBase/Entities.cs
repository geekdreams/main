﻿using System;
using System.ComponentModel.DataAnnotations;
namespace VKCommon.DataBase
{
    public class dbUser
    {
        [Key]
        public string login { get; set; }
        public string password { get; set; }
        public string token { get; set; }
        public string id { get; set; }
    }
}

﻿using System;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;

namespace VKCommon.DataBase
{
    public class ManagerDb
    {
        private Context _ctx;
        public ManagerDb()
        {
            _ctx = new Context();
        }

        public bool SaveToken(dbUser usr)
        {
            try
            {
                _ctx.Users.Add(usr);
                _ctx.SaveChanges();
                return true;
            }
            catch { return false; }
        }

        public string GetTokenUser(string Login)
        {
            var query = from q in _ctx.Users
                        where (q.login == Login)
                        select q.token;
            try
            {
                return query.First();
            }
            catch
            {
                return string.Empty;
            }
        }
    }
}

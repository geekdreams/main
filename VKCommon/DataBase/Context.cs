﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;

namespace VKCommon.DataBase
{
    public class Context : DbContext
    {
        public Context() : base("ConnStr")
        {
            Database.SetInitializer(new System.Data.Entity.DropCreateDatabaseIfModelChanges<Context>()); // Создаем новую Db если контекст изменился
        }
        public DbSet<dbUser> Users { get; set; }

    }
}

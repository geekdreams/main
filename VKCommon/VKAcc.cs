﻿using System;
using System.Text.RegularExpressions;
using xNet;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Diagnostics;
using VKCommon.DataBase;

namespace VKCommon
{// TODO: webRequest.Proxy = null;
    public class VKAcc:VKBase
    {

        //основные поля
        private string _id ;
        public string Id
        {
            get { return _id; }
        }
        private string _login;
        private string _password;
        private string _token;
        public string Token
        {
            get { return _token; }
            set { _token = value; }
        }
        private string _userAgent = UserAgent.Firefox;

        private string _clientId = "3682744";

        private ManagerDb _db;
        //отладочно тестовые поля                         
        private static Stopwatch sw = new Stopwatch();

        #region ctor
        public VKAcc(string login, string password, Uri proxy) //main ctor
        {
            _login = login;
            _password = password;

            if (proxy != null)
            {
                ProxyType pt = ProxyType.Http;
                if (proxy.Scheme == "socks5")
                    pt = ProxyType.Socks5;
                else if (proxy.Scheme == "socks4")
                    pt = ProxyType.Socks4;
                else if (proxy.Scheme == "socks4a")
                    pt = ProxyType.Socks4a;
                else pt = ProxyType.Http;


                _proxy = ProxyClient.Parse(pt, proxy.Authority);
            }
            

            // Work with db
            try
            {
                _db = new ManagerDb();
            }
            catch
            {
                throw new Exception("a problem creating ManageDb see app.conf");
            }

        }
        public VKAcc(string login, string password) : this(login, password, null) { }
        #endregion ctor

        #region Auth
        public void Auth()
        {
            Token = _db.GetTokenUser(_login);
            if (Token == string.Empty)
                if (Authorization())
                    _db.SaveToken(new dbUser
                    {
                        login = _login,
                        password = _password,
                        id = _id,
                        token = GetToken()
                    });
                else throw new Exception("Error login or password");
        }
        private bool Authorization()
        {
            string strResponce;
            string ip_h;
            string lg_h;
            HttpResponse response;    
            _request.Proxy = _proxy;

            // Добавляем headers
            _request[HttpHeader.Accept] = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
            //request. = "gzip"; gzip automatom
            _request[HttpHeader.AcceptLanguage] = "ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3";
            _request.KeepAlive = true;
            _request.UserAgent = _userAgent;
            _request["Upgrade-Insecure-Requests"] = "1";

            // сохраняем кукисы
            response = _request.Get("/", null);// относительный запрос выполнятеся к адресу который задан в конструкторе;

            strResponce = response.ToString();
            _cookies = response.Cookies;
            ip_h = Regex.Match(strResponce, @"(?<=ip_h:\ ').*(?=')").Value;
            lg_h = Regex.Match(strResponce, @"(?<=""lg_h""\ value="").*?(?="")").Value;

            // Параметры для post запроса
            _request.AddParam("_origin", @"https://vk.com")
                    .AddParam("act", "login")
                    .AddParam("captcha_key", "")
                    .AddParam("captcha_sid", "")
                    .AddParam("email", _login)
                    .AddParam("expire", "")
                    .AddParam("ip_h", ip_h)
                    .AddParam("lg_h", lg_h)
                    .AddParam("pass", _password)
                    .AddParam("role", "al_frame");
            // заполняем headers
            _request.Referer = "https://vk.com/";
            _cookies.Add("remixflash", "22.0.0");
            _cookies.Add("remixscreen_depth", "24");
            _cookies.Add("remixdt", "0");

            _request.Cookies = _cookies;
            _request.AllowAutoRedirect = true;
            response = _request.Post("https://login.vk.com/?act=login");

            strResponce = response.ToString();

            _cookies = response.Cookies;

            if (strResponce.Contains("parent.onLoginDone"))
                return true;
            else if (strResponce.Contains("parent.onLoginFailed"))
                return false;
            else return false;
        }
        private string GetToken()
        {
            HttpResponse response;
            string strResponse;
            string url = $"https://oauth.vk.com/authorize?client_id={_clientId}&redirect_uri=https://m.vk.com/&display=mobile&response_type=token&scope=notify,friends,photos,audio,video,docs,notes,pages,status,offers,questions,wall,groups,messages,email,notifications,stats,ads,offline";
            _request.Cookies = _cookies;
            response = _request.Get(url);
            strResponse = response.ToString();
            _cookies = response.Cookies;
            //post
            string postUrl = Regex.Match(strResponse, @"(?<=<form\ method=""post""\ action="")[\w\W]*?(?="">)").Value;
            _request.AddParam("email_denied", "0");
            response = _request.Post(postUrl);

            Uri addres = response.Address;
            _token = Regex.Match(addres.Fragment, @"(?<=token=).*?(?=&)").Value;
            return _token;
        }
        #endregion Auth
        #region VkRequest
        public string VkRequest(string method, TypeRequest typeRequest, bool useToken, bool useProxy, params Param[] param)
        {
            _request.ClearAllHeaders();

            _request.Proxy = useProxy ? _proxy : null;
            string requestUri = $"https://api.vk.com/method/{method}";
            _request.AddUrlParam("v", "5.8");
            if (useToken && _token != "")
                _request.AddUrlParam("access_token", _token);

            JObject res;
            string strResponse;

            if (typeRequest == TypeRequest.GET)
            {
                //for (int i = 0; i < param.Length; i++)
                    _request.AddUrlParam(param);
                    //_request.AddUrlParam(param[i].Name, param[i].Value);
                strResponse = _request.Get(requestUri).ToString();
            }
            else
            {
                // for (int i = 0; i < param.Length; i++)
                _request.AddUrlParam(param);
                strResponse = _request.Post(requestUri).ToString();
            }
            if (strResponse != "")
                res = JObject.Parse(strResponse);
            else res = null;
            return res.ToString();
        }
        public string VkRequest(string method, TypeRequest typeRequest, params Param[] param)
        {
            return VkRequest(method, typeRequest, true, true, param);
        }
        public string VkRequest(string method, bool useToken, bool useProxy, params Param[] param)
        {
            return VkRequest(method, TypeRequest.GET, useToken, useProxy, param);
        }
        public string VkRequest(string method, params Param[] param)
        {
            return VkRequest(method, TypeRequest.GET, true, true, param);
        }
        public string VkRequest(string method)
        {
            return VkRequest(method, TypeRequest.GET, true, true, new Param[] { });
        }
        #endregion VkRequest
    }
}

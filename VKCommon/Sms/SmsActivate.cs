﻿using xNet;
namespace VKCommon.Sms
{
    public abstract class SmsActivate
    {
        protected HttpRequest _request;
        protected TypeRequest _TypeRequest;
        protected string ApiKey;
        public SmsActivate(string ApiKey, TypeRequest tr)
        {
            this.ApiKey = ApiKey;
            _TypeRequest = tr;
            _request = new HttpRequest();
        }
        abstract public double GetBalance();
        abstract public int GetCountNumebers();
        abstract public string GetNumber();
        abstract public bool Banned();
        abstract public string GetSms(int TryCount = 20);
    }
}

﻿using System;
using System.Threading;
using xNet;
using Newtonsoft.Json.Linq;

namespace VKCommon.Sms
{
    public class SimSms : SmsActivate
    {
        string requestUri;
        string numid;
        public SimSms(string ApiKe, TypeRequest tr) : base(ApiKe, tr)
        {
            requestUri = "http://simsms.org/priemnik.php";

        }

        public override double GetBalance()
        {
            _request.AddUrlParam("metod", "get_balance");
            _request.AddUrlParam("service", "opt4");
            _request.AddUrlParam("apikey", ApiKey);
            string strResponce;
            if (_TypeRequest == TypeRequest.GET)
                strResponce = _request.Get(requestUri).ToString();
            else strResponce = _request.Post(requestUri).ToString();
            if (strResponce == "")
                return 0;
            JObject json = JObject.Parse(strResponce);
            return Convert.ToDouble(json["balance"].ToString());
        }
        public override int GetCountNumebers()
        {
            _request.AddUrlParam("metod", "get_count");
            _request.AddUrlParam("service", "opt4");
            _request.AddUrlParam("apikey", ApiKey);
            _request.AddUrlParam("service_id", "vk");

            string strResponce;
            if (_TypeRequest == TypeRequest.GET)
                strResponce = _request.Get(requestUri).ToString();
            else strResponce = _request.Post(requestUri).ToString();
            if (strResponce == "")
                return 0;
            JObject json = JObject.Parse(strResponce);
            return Convert.ToInt32(json["counts Vkontakte"].ToString());
        }

        public override bool Banned()
        {
            throw new NotImplementedException();
        }



        public override string GetNumber()
        {
            _request.AddUrlParam("metod", "get_number");
            //TODO: asd
            _request.AddUrlParam("country", "ru");//!!!!!!!!!1
            _request.AddUrlParam("service", "opt4");
            _request.AddUrlParam("id", "1");
            _request.AddUrlParam("apikey", ApiKey);
            

            string strResponce;
            if (_TypeRequest == TypeRequest.GET)
                strResponce = _request.Get(requestUri).ToString();
            else strResponce = _request.Post(requestUri).ToString();
            if (strResponce == "")
                return string.Empty;
            JObject json = JObject.Parse(strResponce);
            if (json["responce"].ToString() == "1")
            {
                numid = json["id"].ToString(); 
                return json["number"].ToString();
            }
            else
            {
                Thread.Sleep(30000); 
                return this.GetNumber();
            }
        }

        public override string GetSms(int TryCount = 20)
        {
            if (TryCount == 0) return "";

            _request.AddUrlParam("metod", "get_sms");
            _request.AddUrlParam("country", "ru");
            _request.AddUrlParam("service", "opt4");
            _request.AddUrlParam("id", numid);
            _request.AddUrlParam("apikey", ApiKey);
         

            string strResponce;
            if (_TypeRequest == TypeRequest.GET)
                strResponce = _request.Get(requestUri).ToString();
            else strResponce = _request.Post(requestUri).ToString();
            if (strResponce == "")
                return "";
            JObject json = JObject.Parse(strResponce);
            
            if (json["responce"].ToString() == "1")
                return json["sms"].ToString();
            else
            {
                Thread.Sleep(30000);
                return this.GetSms(TryCount-1);
            }
            /*
            http://simsms.org/priemnik.php?
           metod=get_sms
        &country=ru
          &service=opt4
       &id=25623
     &apikey=DSWAFvdedrE4
            */
        }
    }
}

﻿using System;
using xNet;

namespace VKCommon
{
    public enum TypeRequest : byte { GET, POST }

    public enum SmsServices : byte
    {
        smsactivate, smsvk, simsms
    }
    public struct Param
    {
        public string Name;
        public string Value;
        public Param(string name, string value)
        {
            Name = name;
            Value = value;
        }
    }
    public struct UserAgent
    {
        public static readonly string IE11 = @"Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; AS; rv:11.0) like Gecko";
        public static readonly string Chrome = @"Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36";
        public static readonly string Opera = @"Opera/9.80 (X11; Linux i686; Ubuntu/14.10) Presto/2.12.388 Version/12.16";
        public static readonly string Firefox = @"Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1";
    }

    public static class ExtentionMethod
    {
        public static void AddUrlParam(this HttpRequest Request, Param[] param)
        {
            for (int i = 0; i < param.Length; i++)
                Request.AddUrlParam(param[i].Name, param[i].Value);

        }
    }
}

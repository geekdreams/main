﻿using System;
using xNet;
namespace VKCommon.VKRegg
{
    public enum Sex : byte
    {
        male=2,fmale
    }
    public struct VKUser
    {
        public string FName;
        public string LName;
        public Sex Sex;
        public DateTime BirthDay;
        public ProxyClient Proxy;
        public string UserAgent;
    }
}

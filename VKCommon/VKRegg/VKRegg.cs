﻿using System;
using xNet;
using System.Text.RegularExpressions;

using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VKCommon.VKRegg
{
    public class VKRegg : VKBase
    {
        public VKUser User;
        public SmsServices SmsService;
        // public CookieDictionary Cookies;

        public VKRegg(VKUser User)
        {
            this.User = User;
        }
        public VKRegg(string FName, string LName, Sex Sex, DateTime BD)
        {
            this.User = new VKUser
            {
                FName = FName,
                LName = LName,
                Sex = Sex,
                BirthDay = BD
            };
        }

        public void Reggistration()
        {
            // TODO: Необходимо логировать ответы от сервера в XML для последующей отладки (Не действительный номер и тп)
            HttpResponse response;
            string resp;
            // подготавливаем основные headers
            _request[HttpHeader.Accept] = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
            _request[HttpHeader.AcceptLanguage] = "ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3";
            _request.KeepAlive = true;
            _request["Upgrade-Insecure-Requests"] = "1";
            _request.UserAgent = User.UserAgent;
            _request.Cookies = new CookieDictionary(); // TODO: Сделать тоже в Acc
            //Переходим на главную получаем куки
            response = _request.Get("http://m.vk.com/join");


            //Переходим на страницу регистрации

            _request.AddHeader(HttpHeader.Referer, "http://m.vk.com/");
            //отправляем пост запрос
            string stcontent = "_nlm=1&_ref=";

            response = _request.Post("https://vk.com/join.php?act=start", stcontent, "application/x-www-form-urlencoded");
            _cookies = response.Cookies;
            resp = response.ToString();
            // нажимаем кнопку зарегестрироваться           
            _request.AddHeader(HttpHeader.Referer, "http://m.vk.com/join");
            _request.AddHeader("X-Requested-With", "XMLHttpRequest");

            _request.AddUrlParam("act", "start");
            stcontent = $"_nlm=1&_ref=join&act=start&first_name={User.FName}&last_name={User.LName}&sex=0";
            response = _request.Post("https://m.vk.com/join", stcontent, "application/x-www-form-urlencoded");
            resp = response.ToString();
            string m = Regex.Match(resp, @"(?<=m=).*(?=&)").Value;
            string mpd = Regex.Match(resp, @"(?<=mpd=).*(?=&)").Value;
            _request.AddParam("m", m);
            _request.AddParam("mpd", mpd);
            stcontent = $"m={m}&mpd={mpd}&_nlm=1&_ref=join";

            response = _request.Post("https://m.vk.com/join", stcontent, "application/x-www-form-urlencoded");
            resp = response.ToString();


            //_request.Cookies = _cookies;
            _request.AddHeader(HttpHeader.Referer, $"http://m.vk.com/join?m={m}&mpd={mpd}");
            _request.AddHeader("X-Requested-With", "XMLHttpRequest");

            _request.AddUrlParam("act", "start");
            stcontent = $"act=start&first_name={User.FName}&last_name={User.LName}&sex={Convert.ToString((int)User.Sex)}&_nlm=1&_ref=join";
            response = _request.Post("https://m.vk.com/join", stcontent, "application/x-www-form-urlencoded");
            resp = response.ToString();

            //Разгадать капчу
            string captcha_sid = Regex.Match(resp, @"(?<=sid=).*?(?=\&)").Value;
            if (captcha_sid != "")
            {
                _request.AddHeader(HttpHeader.Accept, "*/*");
                _request.Referer = "http://m.vk.com/join?act=start";
                response = _request.Get($"http://m.vk.com/captcha.php?sid={captcha_sid}&s=1");
                response.ToFile(@"C:\ZL.tmp\1.jpeg");
                _request.AddHeader(HttpHeader.Referer, $"http://m.vk.com/join?act=start");
                _request.AddUrlParam("act", "start");
                Console.WriteLine(@"Введите капчу с папки C:\ZL.tmp\");
                string key = Console.ReadLine();
                stcontent += $"&captcha_sid={captcha_sid}&captcha_key={key}";
                // _request.AllowAutoRedirect = false; // для теста отключаем редик
                response = _request.Post("https://m.vk.com/join", stcontent, "application/x-www-form-urlencoded"); // тут ответ 302 редик
                resp = response.ToString();
                // добавим headers

                //_request.AddHeader(HttpHeader.Referer, "http://m.vk.com/join?act=start");
                //response = _request.Get("http://m.vk.com/join?act=finish");
                //resp = response.ToString();
            }

            string hash = Regex.Match(resp, @"(?<=phone\&hash=).*?(?="")").Value;
            if (hash == "") throw new Exception("Не удалось спарсить hash не правильно введена капча");

            // Отправка запроса на смс
            string phonenumber = Http.UrlEncode("+79145027168");
            _request.AddHeader(HttpHeader.Referer, "http://m.vk.com/join?act=finish");
            _request.AddHeader("X-Requested-With", "XMLHttpRequest");

            _request.AddUrlParam("act", "phone");
            _request.AddUrlParam("hash", hash);

            stcontent = $"act=phone&hash={hash}&phone={phonenumber}&_nlm=1&_ref=join";

            response = _request.Post("https://m.vk.com/join", stcontent, "application/x-www-form-urlencoded");
            resp = response.ToString();
            // Получили смс // resp =  Ответ [1068,false,1,"\/join?act=finish"]

           
            _request.AddHeader(HttpHeader.Referer, "http://m.vk.com/join?act=finish");
            _request.AddHeader("X-Requested-With", "XMLHttpRequest");

            _request.AddUrlParam("act", "finish");
            stcontent = $"act=finish&_nlm=1&_ref=join";

            response = _request.Post("https://m.vk.com/join", stcontent, "application/x-www-form-urlencoded");
            resp = response.ToString(); // Не обезателньо

            // Отправляем запрос с кодом из смс
            _request.AddHeader(HttpHeader.Referer, "http://m.vk.com/join?act=finish");
            _request.AddHeader("X-Requested-With", "XMLHttpRequest");

            _request.AddParam("act", "check_code");
            _request.AddParam("hash", hash);

            string smscode = "";// получаем код из смс
            Console.WriteLine("Введите код полученый из смс");
            smscode = Console.ReadLine();
            stcontent = $"act=check_code&hash={hash}&phone={phonenumber}&code={smscode}&_nlm=1&_ref=join";
            response = _request.Post("https://m.vk.com/join", stcontent, "application/x-www-form-urlencoded");

            resp = response.ToString(); //[1068,false,1,"\/join?act=finish"] Не обезательно

            // Получаем https://login.vk.com/?act=login&_origin=http://m.vk.com&ip_h=2c29610cbebbf55bf8&lg_h=8f9a1c89094b5c70a2&role=pda&join_code=36751&join_hash=10d5a7c069857b35d0c54a4abf704639&to=am9pbj9hY3Q9ZG9uZQ--

            _request.AddHeader(HttpHeader.Referer, "http://m.vk.com/join?act=finish"); // TODO: Сделать хидеры перманентные
            _request.AddHeader("X-Requested-With", "XMLHttpRequest");

            _request.AddUrlParam("act", "finish");
            stcontent = $"act=finish&_nlm=1&_ref=join";

            response = _request.Post("https://m.vk.com/join", stcontent, "application/x-www-form-urlencoded");
            resp = response.ToString();
            // Парсим URL
            string urlpassword = Regex.Match(resp, @"https:\\/\\/login.vk.com\\/\?act=login.*?(?="" >)").Value;
            urlpassword = urlpassword.Replace("\\", "");


            // Отсылаем пароль
            string password;
            Console.WriteLine("Введите пароль");
            password = Console.ReadLine();
            password = Http.UrlEncode(password);
            _request.AddHeader(HttpHeader.Referer, "http://m.vk.com/join?act=finish");
            _request.AddHeader("X-Requested-With", "XMLHttpRequest");

            stcontent = $"email={phonenumber}&pass={password}";
            response = _request.Post(urlpassword,stcontent, "application/x-www-form-urlencoded");
            resp = response.ToString();

            Console.WriteLine();

            //&al=1&bday=5&bmonth=2&byear=1993&fname=Lname&frm=1&lname=Fname&sex=2")


        }
    }
}
